# mathfast
About
Simple calculator for making math expressions fast in the terminal
## To install run
---
```bash
sudo make install
```

<details>
    <summary><h2>Preview</h2></summary>
    <div align="center">
        <img src="preview.png" alt="preview">
    </div>
</details>