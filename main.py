#!/usr/bin/python3
import sys

if len(sys.argv) > 1 and sys.argv[1] != "":
    try:
        print(eval(sys.argv[1]))
    except Exception as e:
        print("Something went wrong")
else:
    try:
        print(eval(input()))
    except Exception as e:
        print("Something went wrong")
